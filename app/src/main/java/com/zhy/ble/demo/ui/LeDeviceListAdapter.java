package com.zhy.ble.demo.ui;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zhy.ble.demo.R;

import java.util.ArrayList;
import java.util.List;

import cn.com.heaton.bleLib.ble.model.BleDevice;


/**
 * Created by LiuLei on 2016/11/26.
 */
public class LeDeviceListAdapter extends BaseAdapter {
    private ArrayList<BleDevice> mLeDevices;
    private LayoutInflater mInflator;

    public LeDeviceListAdapter(Activity context) {
        super();
        mLeDevices = new ArrayList<BleDevice>();
        mInflator = context.getLayoutInflater();
    }

    public void addDevice(BleDevice device) {
        mLeDevices.add(device);
    }

    public void addDevices(List<BleDevice> devices) {
        mLeDevices.addAll(devices);
    }

    public BleDevice getDevice(int position) {
        return mLeDevices.get(position);
    }

    public void clear() {
        mLeDevices.clear();
    }

    @Override
    public int getCount() {
        return mLeDevices == null ? 0 : mLeDevices.size();
    }

    @Override
    public Object getItem(int i) {
        return mLeDevices == null || mLeDevices.size() == 0 ? null : mLeDevices.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder viewHolder;
        // General ListView optimization code.
        if (view == null) {
            view = mInflator.inflate(R.layout.listitem_device, null);
            viewHolder = new ViewHolder();
            viewHolder.deviceAddress = (TextView) view.findViewById(R.id.device_address);
            viewHolder.deviceName = (TextView) view.findViewById(R.id.device_name);
//            viewHolder.deviceState = (TextView) view.findViewById(R.id.state);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        final BleDevice device = mLeDevices.get(i);
        final String deviceName = device.getBleName();
        final String deviceRSSI = BluetoothDevice.EXTRA_RSSI;
//        if (device.isConnectting()) {
//            viewHolder.deviceState.setText("正在连接中...");
//        } else if (device.isConnected()) {
//            viewHolder.deviceState.setText("已连接");
//        } else {
//            viewHolder.deviceState.setText("未连接");
//        }
        if (deviceName != null && deviceName.length() > 0)
            viewHolder.deviceName.setText(deviceName);
        else if (deviceRSSI != null && deviceRSSI.length() > 0) {
        }
//            viewHolder.deviceRSSI.setText(deviceRSSI);
//        else
//            viewHolder.deviceName.setText("Unknown device");
        viewHolder.deviceAddress.setText(device.getBleAddress() + "");

        return view;
    }

    class ViewHolder {
        TextView deviceName;
        TextView deviceAddress;
//        TextView deviceRSSI;
//        TextView deviceState;
    }

}
