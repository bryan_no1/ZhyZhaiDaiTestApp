package com.zhy.ble.demo;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.bryan.autolayout.config.AutoLayoutConifg;
import com.bryan.common.utils.AppManager;
import com.bryan.common.utils.MMKVUtil;
import com.orhanobut.logger.LogBuilder;
import com.orhanobut.logger.Logger;
import com.tencent.bugly.crashreport.CrashReport;

import java.util.List;


public class MyApplication extends MultiDexApplication {
    private static final String TAG = "MyApplication";
    private static MyApplication _App;

    public int activityCount = 0;

    @Override
    public void onCreate() {
        super.onCreate();

        String currentProcessName = getCurrentProcessName(getApplicationContext());
        if (getApplicationInfo().packageName.equals(currentProcessName)) {
            _App = (MyApplication) getApplicationContext();
            //文件目录初始化
            MnsFileUtil.initFilePath(_App);
            //Crash机制
            CrashHandler.getInstance().init(this);
            //Bugly初始化
            initBugly();
            //使用设备物理尺寸（包括状态栏和导航栏）
            AutoLayoutConifg.getInstance().useDeviceSize();
            ToastMgr.init(getApplicationContext());

            //init MMKV
            MMKVUtil.init(getApplicationContext());

            //init log
            Logger.initialize(
                    LogBuilder.create()
//                          .logPrintStyle(new XLogStyle())
                            .showMethodLink(false)
                            .showThreadInfo(false)
                            .tagPrefix("bryan")
//                          .globalTag("globalTag")
                            .methodOffset(1)
                            .logPriority(BuildConfig.IsDebug ? Log.VERBOSE : Log.ASSERT)
                            .logPriority(Log.VERBOSE)
                            .build()

            );

            //注册Activity的生命周期,在这边手动管理每一个Activity，方便需要的时候退出
            registerActivityLifecycleCallbacks(new Application.ActivityLifecycleCallbacks() {
                @Override
                public void onActivityCreated(Activity activity, Bundle bundle) {
                    AppManager.getAppManager().addActivity(activity);
                }

                @Override
                public void onActivityStarted(Activity activity) {
                    activityCount++;
                }

                @Override
                public void onActivityResumed(Activity activity) {

                }

                @Override
                public void onActivityPaused(Activity activity) {

                }

                @Override
                public void onActivityStopped(Activity activity) {
                    activityCount--;
                }

                @Override
                public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

                }

                @Override
                public void onActivityDestroyed(Activity activity) {

                    AppManager.getAppManager().removeActivity(activity);

                }
            });


        }
    }


    public static MyApplication get() {
        return _App;
    }


    /**
     * 获取当前进程名称
     *
     * @return processName
     */
    public String getCurrentProcessName(Context context) {
        int currentProcessId = android.os.Process.myPid();
        ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> runningAppProcesses = activityManager.getRunningAppProcesses();
        for (ActivityManager.RunningAppProcessInfo runningAppProcess : runningAppProcesses) {
            if (runningAppProcess.pid == currentProcessId) {
                return runningAppProcess.processName;
            }
        }
        return null;
    }


    protected void initBugly() {
        if (BuildConfig.IsDebug) {
            //如果是测试版就设置成测试设备
            CrashReport.setIsDevelopmentDevice(this, BuildConfig.IsDebug);
        }
        CrashReport.initCrashReport(this, "2cd6febaa2", BuildConfig.IsDebug);
    }

}
