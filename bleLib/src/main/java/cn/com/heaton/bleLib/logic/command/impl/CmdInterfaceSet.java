package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.command.Command;

/**
 * 内外置接口设置
 */
public class CmdInterfaceSet extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private byte[] mSendBuffer;

    public CmdInterfaceSet(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback, boolean isInModel, boolean isATModel) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        initData(isInModel, isATModel);
    }

    private void initData(boolean isInModel, boolean isATModel) {
        byte[] cmd = new byte[3];
        cmd[0] = Cmd_Lora_In_Out;
        if (isInModel) {
            cmd[1] = 0x00;
        } else {
            cmd[1] = 0x01;
        }

        if (isATModel) {
            cmd[2] = 0x00;
        } else {
            cmd[2] = 0x01;
        }

        mSendBuffer = combineData(cmd, cmd.length);
    }

    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条获取设置Lora 内置外置的指令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
