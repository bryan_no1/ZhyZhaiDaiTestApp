package cn.com.heaton.bleLib.ble.request;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import cn.com.heaton.bleLib.ble.callback.BleChaChangeCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.utils.TaskExecutor;
import cn.com.heaton.bleLib.ble.annotation.Implement;
import cn.com.heaton.bleLib.ble.callback.BleNotiftCallback;
import cn.com.heaton.bleLib.ble.callback.wrapper.NotifyWrapperLisenter;
import cn.com.heaton.bleLib.logic.HexUtil;

/**
 * 这个Notify只是做回调通知而已
 * Created by LiuLei on 2017/10/23.
 */
@Implement(NotifyRequest.class)
public class NotifyRequest<T extends BleDevice> implements NotifyWrapperLisenter<T> {

    private static final String TAG = "NotifyRequest";

    private BleNotiftCallback<T> mBleLisenter;

    private List<BleChaChangeCallback<T>> mBleChaChangeListeners = new ArrayList<>();
    private HashMap<T, BleNotiftCallback> mBleNotifyMap = new HashMap<>();
    private List<BleNotiftCallback> mNotifyCallbacks = new ArrayList<>();
    private HashMap<T, List<BleNotiftCallback>> mBleNotifyMaps = new HashMap<>();

    protected NotifyRequest() {
    }

    /**
     * 这里notify其实只是添加通知回调而已,在连接成功的时候回调一下
     * SDK发现了设备之后，会自动对设备进行设置通知
     *
     * @param device
     * @param callback
     */
    public void notify(T device, BleNotiftCallback<T> callback) {
//        if(callback != null && !mNotifyCallbacks.contains(callback)){
//            this.mNotifyCallbacks.add(callback);
//        }
//        if(!mBleNotifyMap.containsKey(device)){
//            this.mBleNotifyMap.put(device, callback);
//            this.mNotifyCallbacks.add(callback);
//        }
        if (!mNotifyCallbacks.contains(callback)) {
            List<BleNotiftCallback> bleCallbacks;
            if (mBleNotifyMaps.containsKey(device)) {
                bleCallbacks = mBleNotifyMaps.get(device);
                bleCallbacks.add(callback);
            } else {//不包含key
                bleCallbacks = new ArrayList<>();
                bleCallbacks.add(callback);
                mBleNotifyMaps.put(device, bleCallbacks);
            }
            mNotifyCallbacks.add(callback);
        }
    }

    /**
     * 设备断开的时候就会自动移除通知
     *
     * @param device
     */
    public void unNotify(T device) {
//        if(callback != null && mNotifyCallbacks.contains(callback)){
//            this.mNotifyCallbacks.remove(callback);
//        }
//        if(mBleNotifyMap.containsKey(device)){
//            mNotifyCallbacks.remove(mBleNotifyMap.get(device));
//            mBleNotifyMap.remove(device);
//        }
        if (mBleNotifyMaps.containsKey(device)) {
            //移除该设备的所有通知
            mNotifyCallbacks.removeAll(mBleNotifyMaps.get(device));
            mBleNotifyMaps.remove(device);
        }
    }

    public void addChaChangeListener(BleChaChangeCallback callback) {
        if (!mBleChaChangeListeners.contains(callback)) {
            L.e(TAG, "注册了特征值变化的监听");
            mBleChaChangeListeners.add(callback);
        }
    }

    @Override
    public void onChanged(final T device, final BluetoothGattCharacteristic characteristic) {
        if (characteristic == null) {
            L.i(TAG, "characteristic  is null");
        } else {
            final byte[] resultData = characteristic.getValue();
            Log.i(TAG, " ----->   characteristic value " + HexUtil.encodeHexStr(resultData));

            TaskExecutor.mainThread(new Runnable() {
                @Override
                public void run() {

                    for (BleChaChangeCallback callback : mBleChaChangeListeners) {
                        callback.onChanged(device, resultData);
                    }
                }
            });
            TaskExecutor.mainThread(new Runnable() {
                @Override
                public void run() {

                    for (BleNotiftCallback callback : mNotifyCallbacks) {
                        callback.onChanged(device, characteristic);
                    }
                }
            });


        }


    }

    @Override
    public void onReady(T device) {

    }

    @Override
    public void onServicesDiscovered(final BluetoothGatt gatt) {
        TaskExecutor.mainThread(new Runnable() {
            @Override
            public void run() {
                for (BleNotiftCallback callback : mNotifyCallbacks) {
                    callback.onServicesDiscovered(gatt);
                }
            }
        });
    }

    @Override
    public void onNotifySuccess(final BluetoothGatt gatt) {
        TaskExecutor.mainThread(new Runnable() {
            @Override
            public void run() {
                for (BleNotiftCallback callback : mNotifyCallbacks) {
                    callback.onNotifySuccess(gatt);
                }
            }
        });
    }
}
