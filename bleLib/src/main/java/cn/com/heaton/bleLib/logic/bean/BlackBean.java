package cn.com.heaton.bleLib.logic.bean;

import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.logic.HexUtil;

public class BlackBean {


    public int cardLength;//卡号长度
    public byte[] cardNo;//卡号，长度为卡号的长度


    public int year;//年
    public int month;
    public int day;
    public String formateTime; //有效期

    /**
     * 数据解析
     *
     * @param data
     * @param offset
     * @return
     */
    public static BlackBean of(byte[] data, int offset) {

        BlackBean blackBean = new BlackBean();
        L.i("BlackBean", ">>>:" + HexUtil.encodeHexStr(data));
        offset++;//跳过卡类型
        //卡号长度
        byte length = data[offset];
        blackBean.cardLength = Integer.parseInt(HexUtil.Byte2Hex(length), 16);
        offset++;

        //卡号
        byte[] cardNo = new byte[length];
        System.arraycopy(data, offset, cardNo, 0, length);
        blackBean.cardNo = cardNo;
        offset = offset + length;


        //year
        byte yearB = data[offset];
        blackBean.year = Integer.parseInt(HexUtil.Byte2Hex(yearB), 16) + 2000;
        offset++;

        //month
        byte month = data[offset];
        blackBean.month = Integer.parseInt(HexUtil.Byte2Hex(month), 16);
        offset++;

        //day

        byte day = data[offset];
        blackBean.day = Integer.parseInt(HexUtil.Byte2Hex(day), 16);
        offset++;

        blackBean.formateTime();
        return blackBean;
    }

    public void formateTime() {
        StringBuilder sbTime = new StringBuilder();
        sbTime.append(year)
                .append("年")
                .append(month)
                .append("月")
                .append(day)
                .append("日");
        formateTime = sbTime.toString();
    }
}
