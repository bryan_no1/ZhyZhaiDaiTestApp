package cn.com.heaton.bleLib.logic.command;

/**
 * 指令队列
 * Created by LiuLei on 2017/11/6.
 */
public interface CommandQueue {

    /**
     * 添加一条指令
     *
     * @param command
     */
    void add(Command command);

    /**
     * 完成一条指令
     */
    void next();

    /**
     * 指令队列中是否还有内容
     *
     * @return
     */
    boolean isEmpty();

}
