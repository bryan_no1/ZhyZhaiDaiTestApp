package cn.com.heaton.bleLib.logic.bean;

import java.sql.Time;

import cn.com.heaton.bleLib.logic.HexUtil;

public class TimeBean {

    public int year;
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int second;
    public String formateTime;


    public void formateTime() {
        StringBuilder sbTime = new StringBuilder();
        sbTime.append(year)
                .append("年")
                .append(month)
                .append("月")
                .append(day)
                .append("日")
                .append(" ")
                .append(hour)
                .append("时")
                .append(minute)
                .append("分")
                .append(second)
                .append("秒");
        formateTime = sbTime.toString();
    }


    /**
     * @param data   原始数据
     * @param offset 从第index 开始就是时间数据，自己解析
     * @return
     */
    public static TimeBean parseTimeBean(byte[] data, int offset) {
        TimeBean timeBean = new TimeBean();

        //year
        byte[] yearB = new byte[2];
        yearB[1] = data[offset];
        offset++;
        yearB[0] = data[offset];
        timeBean.year = Integer.parseInt(HexUtil.encodeHexStr(yearB), 16);
        offset++;

        //month
        byte month = data[offset];
        timeBean.month = Integer.parseInt(HexUtil.Byte2Hex(month), 16);
        offset++;

        //day

        byte day = data[offset];
        timeBean.day = Integer.parseInt(HexUtil.Byte2Hex(day), 16);
        offset++;

        //hour
        byte hour = data[offset];
        timeBean.hour = Integer.parseInt(HexUtil.Byte2Hex(hour), 16);
        offset++;

        //minute
        byte minute = data[offset];
        timeBean.minute = Integer.parseInt(HexUtil.Byte2Hex(minute), 16);
        offset++;

        //secont
        byte secont = data[offset];
        timeBean.second = Integer.parseInt(HexUtil.Byte2Hex(secont), 16);
        offset++;

        //默认的格式化时间
        timeBean.formateTime();


        return timeBean;
    }
}
