package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.HexUtil;
import cn.com.heaton.bleLib.logic.command.Command;

/**
 * 查询设备白名单
 */
public class CmdDeviceBlack extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private byte[] mSendBuffer;

    public CmdDeviceBlack(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback, int index) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        initData(index);
    }

    private void initData(int index) {
        byte[] cmd = new byte[5];
        cmd[0] = Cmd_Read_Black;

        /**
         * 直接将10进制转成16进制，然后转成4个字节的长度
         *          String  trHex = Integer.toHexString(valueTen);
         *         System.out.println(valueTen + " [十进制]---->[十六进制] " + strHex);
         *         //将十六进制格式化输出
         *         String strHex2 = String.format("%08x",valueTen);
         */
        //转成16进制hex,格式化成4个字节的数据
        String formatHex = String.format("%08x", index);

        //拼装数据
        byte[] data = HexUtil.HexToByteArr(formatHex);
        data = HexUtil.reverse(data);

        System.arraycopy(data, 0, cmd, 1, data.length);


        mSendBuffer = combineData(cmd, cmd.length);
    }

    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条获取设备黑名单的指令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
