package cn.com.heaton.bleLib.logic.notify;

import cn.com.heaton.bleLib.logic.bean.TimeBean;

/**
 * Created by bryan on 2019/5/21.
 */
public abstract class FetchTimeCallBack {

    public abstract void onSuccess(TimeBean timeBean);

    public abstract void onFail(String failReason);
}
