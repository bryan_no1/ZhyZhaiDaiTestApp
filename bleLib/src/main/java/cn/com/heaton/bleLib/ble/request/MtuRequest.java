package cn.com.heaton.bleLib.ble.request;

import android.os.Message;

import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.ble.BleHandler;
import cn.com.heaton.bleLib.ble.BleStates;
import cn.com.heaton.bleLib.ble.BluetoothLeService;
import cn.com.heaton.bleLib.ble.annotation.Implement;
import cn.com.heaton.bleLib.ble.callback.BleMtuCallback;

/**
 *
 * Created by LiuLei on 2017/10/23.
 */
@Implement(MtuRequest.class)
public class MtuRequest<T extends BleDevice> implements IMessage {

    private BleMtuCallback<T> mBleLisenter;

    protected MtuRequest() {
        BleHandler handler = BleHandler.of();
        handler.setHandlerCallback(this);
    }

    public boolean setMtu(String address, int mtu, BleMtuCallback<T> lisenter){
        this.mBleLisenter = lisenter;
        boolean result = false;
        BluetoothLeService service = Ble.getInstance().getBleService();
        if (Ble.getInstance() != null && service != null) {
            result = service.setMTU(address, mtu);
        }
        return result;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case BleStates.BleStatus.MTUCHANGED:
                if(msg.obj instanceof BleDevice){
                    BleDevice device = (BleDevice) msg.obj;
                    if(mBleLisenter != null){
                        mBleLisenter.onMtuChanged(device, msg.arg1, msg.arg2);
                    }
                }
                break;
            default:
                break;
        }
    }
}
