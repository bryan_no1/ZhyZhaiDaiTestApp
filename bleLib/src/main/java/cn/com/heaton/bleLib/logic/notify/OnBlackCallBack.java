package cn.com.heaton.bleLib.logic.notify;

import cn.com.heaton.bleLib.logic.bean.BlackBean;
import cn.com.heaton.bleLib.logic.bean.WhiteBean;

/**
 * 白名单回调
 * Created by bryan on 2019/5/21.
 */
public abstract class OnBlackCallBack {

    public abstract void onSuccess(BlackBean blackBean);

    public abstract void onFail(String failReason);
}
