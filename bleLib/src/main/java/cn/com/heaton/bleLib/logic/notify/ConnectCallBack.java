package cn.com.heaton.bleLib.logic.notify;

/**
 * Created by bryan on 2019/5/21.
 */
public abstract class ConnectCallBack {

   public abstract void connectSuccess();

    public abstract void connectFail(String desc);
}
