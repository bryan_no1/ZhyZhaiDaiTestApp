package cn.com.heaton.bleLib.ble.request;

import android.os.Message;

/**
 *
 * Created by LiuLei on 2017/12/28.
 */
public interface IMessage {
    void handleMessage(Message msg);
}
