package cn.com.heaton.bleLib.logic.bean;

import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.logic.HexUtil;

/**
 * 设备信息
 */
public class DeviceInfo {


    //数据为16进制格式，低字节在前，高字节在后。

    public int deviceCode;//设备号
    public byte[] deviceNo;//设备编号，4个字节
    public String deviceNOFormat;//16进制，Hex展示
    public int loraModel;//Lora模式;0x00为内置，0x01为外置。
    public byte[] workVoltage;//工作电压，2个字节
    public byte[] batteryVoltage;//电池电压，2个字节
    public byte[] whiteNo;//白名单数量,4个字节，直接Hex 16进制 转 10进制
    public int whiteNoInt;//
    public byte[] blackNo;//黑名单数量，4个字节 直接Hex 16进制 转 10进制
    public int blackNoInt;
    public byte[] recordNo;//刷卡记录数量 直接Hex 16进制 转 10进制
    public int recordNoInt;


    /**
     * 数据解析
     *
     * @param data
     * @param offset
     * @return
     */
    public static DeviceInfo of(byte[] data, int offset) {

        L.i("DeviceInfo", ">>>:" + HexUtil.encodeHexStr(data));

        DeviceInfo deviceInfo = new DeviceInfo();
        //设备号
        byte code = data[offset];
        deviceInfo.deviceCode = Integer.parseInt(HexUtil.Byte2Hex(code), 16);
        offset++;

        //项目编号
        byte[] no = new byte[4];
        no[3] = data[offset];
        offset++;

        no[2] = data[offset];
        offset++;
        no[1] = data[offset];
        offset++;
        no[0] = data[offset];
        offset++;
        deviceInfo.deviceNo = no;
        deviceInfo.deviceNOFormat = HexUtil.encodeHexStr(no);


        //Lora模式
        byte model = data[offset];
        deviceInfo.loraModel = Integer.parseInt(HexUtil.Byte2Hex(model), 16);
        offset++;

        //工作电压
        byte[] work = new byte[2];
        work[1] = data[offset];
        offset++;
        work[0] = data[offset];
        offset++;
        deviceInfo.workVoltage = work;

        //电池电压
        byte[] battery = new byte[2];
        battery[1] = data[offset];
        offset++;
        battery[0] = data[offset];
        offset++;
        deviceInfo.batteryVoltage = battery;

        //白名单数量
        byte[] white = new byte[4];
        white[3] = data[offset];
        offset++;
        white[2] = data[offset];
        offset++;
        white[1] = data[offset];
        offset++;
        white[0] = data[offset];
        offset++;
        deviceInfo.whiteNo = white;
        deviceInfo.whiteNoInt = Integer.parseInt(HexUtil.encodeHexStr(white), 16);


        //黑名单数量
        byte[] black = new byte[4];
        black[3] = data[offset];
        offset++;
        black[2] = data[offset];
        offset++;
        black[1] = data[offset];
        offset++;
        black[0] = data[offset];
        offset++;
        deviceInfo.blackNo = black;
        deviceInfo.blackNoInt = Integer.parseInt(HexUtil.encodeHexStr(black), 16);


        //刷卡记录
        byte[] record = new byte[4];

        record[3] = data[offset];
        offset++;
        record[2] = data[offset];
        offset++;
        record[1] = data[offset];
        offset++;
        record[0] = data[offset];
        offset++;


        deviceInfo.recordNo = record;
        deviceInfo.recordNoInt = Integer.parseInt(HexUtil.encodeHexStr(record), 16);

        return deviceInfo;
    }
}







