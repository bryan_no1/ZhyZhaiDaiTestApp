package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.command.Command;

/**
 * 获取设备时间
 */
public class CmdFetchTime extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private byte[] mSendBuffer;

    public CmdFetchTime(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        initData();
    }

    private void initData() {
        byte[] cmd = new byte[1];
        cmd[0] = 0x07;
        mSendBuffer = combineData(cmd, cmd.length);
    }

    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条获取设备时间指令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
