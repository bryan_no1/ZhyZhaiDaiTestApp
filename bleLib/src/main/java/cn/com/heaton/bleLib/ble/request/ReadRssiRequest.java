package cn.com.heaton.bleLib.ble.request;

import android.os.Message;

import cn.com.heaton.bleLib.ble.BleHandler;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.BleStates;
import cn.com.heaton.bleLib.ble.BluetoothLeService;
import cn.com.heaton.bleLib.ble.annotation.Implement;
import cn.com.heaton.bleLib.ble.callback.BleReadRssiCallback;

/**
 *
 * Created by LiuLei on 2017/10/23.
 */
@Implement(ReadRssiRequest.class)
public class ReadRssiRequest<T extends BleDevice> implements IMessage {

    private BleReadRssiCallback<T> mBleLisenter;

    protected ReadRssiRequest() {
        BleHandler handler = BleHandler.of();
        handler.setHandlerCallback(this);
    }

    public boolean readRssi(T device, BleReadRssiCallback<T> lisenter){
        this.mBleLisenter = lisenter;
        boolean result = false;
        BluetoothLeService service = Ble.getInstance().getBleService();
        if (Ble.getInstance() != null && service != null) {
            result = service.readRssi(device.getBleAddress());
        }
        return result;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what){
            case BleStates.BleStatus.ReadRssi:
                if(msg.obj instanceof Integer){
                    int rssi = (int) msg.obj;
                    if(mBleLisenter != null){
                        mBleLisenter.onReadRssiSuccess(rssi);
                    }
                }
                break;
            default:
                break;
        }
    }
}
