package cn.com.heaton.bleLib.logic.bean;

import cn.com.heaton.bleLib.R;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.logic.HexUtil;

/**
 * 开门记录解析类
 * <p>
 * 开门类型
 * 1楼宇对讲
 * 2门禁卡
 * 3蓝牙
 * 4密码
 * 5身份证
 * 6出门按钮
 * 7APP远程开门
 * 8门禁平台
 * 9二维码
 */
public class RecordBean {

    public int recordType;//开门记录类型
    public int year;//年
    public int month;
    public int day;
    public int hour;
    public int minute;
    public int second;
    public String formateTime;
    public int cardLength;//卡号长度
    public byte[] cardNo;//卡号


    public static RecordBean of(byte[] data, int offset) {

        L.i("RecordBean", ">>>:" + HexUtil.encodeHexStr(data));
        RecordBean recordBean = new RecordBean();
        //开门类型
        byte recordTypeB = data[offset];
        recordBean.recordType = Integer.parseInt(HexUtil.Byte2Hex(recordTypeB), 16);
        offset++;

        //year
        byte yearB = data[offset];
        recordBean.year = Integer.parseInt(HexUtil.Byte2Hex(yearB), 16) + 2000;
        offset++;

        //month
        byte month = data[offset];
        recordBean.month = Integer.parseInt(HexUtil.Byte2Hex(month), 16);
        offset++;

        //day
        byte day = data[offset];
        recordBean.day = Integer.parseInt(HexUtil.Byte2Hex(day), 16);
        offset++;

        //hour
        byte hour = data[offset];
        recordBean.hour = Integer.parseInt(HexUtil.Byte2Hex(hour), 16);
        offset++;

        //minute
        byte minute = data[offset];
        recordBean.minute = Integer.parseInt(HexUtil.Byte2Hex(minute), 16);
        offset++;

        //secont
        byte secont = data[offset];
        recordBean.second = Integer.parseInt(HexUtil.Byte2Hex(secont), 16);
        offset++;

        //默认的格式化时间
        recordBean.formateTime();

        //卡号长度
        byte length = data[offset];
        recordBean.cardLength = Integer.parseInt(HexUtil.Byte2Hex(length), 16);
        offset++;

        //卡号
        byte[] cardNo = new byte[length];
        System.arraycopy(data, offset, cardNo, 0, length);
        recordBean.cardNo = cardNo;
        offset = offset + length;


        return recordBean;

    }


    public void formateTime() {
        StringBuilder sbTime = new StringBuilder();
        sbTime.append(year)
                .append("年")
                .append(month)
                .append("月")
                .append(day)
                .append("日")
                .append(" ")
                .append(hour)
                .append("时")
                .append(minute)
                .append("分")
                .append(second)
                .append("秒");
        formateTime = sbTime.toString();
    }
}
