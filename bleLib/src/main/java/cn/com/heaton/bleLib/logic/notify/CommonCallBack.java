package cn.com.heaton.bleLib.logic.notify;


/**
 * 通用返回接口
 * Created by bryan on 2019/5/21.
 */
public abstract class CommonCallBack {

    public abstract void onSuccess(String desc);

    public abstract void onFail(String reason);


}
