package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.command.Command;

/**
 * 请求进行设备授权，设备连接的时候需要完成此次认证
 */
public class AuthRequest extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private byte[] mSendBuffer;

    public AuthRequest(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        initData();
    }

    private void initData() {
        byte[] cmd = new byte[4];
        cmd[0] = (byte) 0xFA;
        cmd[1] = (byte) 0xFA;
        cmd[2] = (byte) 0xFA;
        cmd[3] = (byte) 0xFA;
        mSendBuffer = cmd;
    }

    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条请求授权命令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
