package cn.com.heaton.bleLib.logic.notify;

import cn.com.heaton.bleLib.logic.bean.DeviceInfo;

/**
 * 查询设备信息
 * Created by bryan on 2019/5/21.
 */
public abstract class DeviceInfoCallBack {

    public abstract void onSuccess(DeviceInfo timeBean);

    public abstract void onFail(String failReason);
}
