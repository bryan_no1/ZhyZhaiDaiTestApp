package cn.com.heaton.bleLib.logic;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;

/**
 * Created by bryan on 2018/1/5 0005.
 */

public class DES3Utils {
    // 定义加密算法，DESede即3DES
    private static final String Algorithm = "DESede/ECB/NoPadding";// 加密密钥
    private static final String PASSWORD_CRYPT_KEY = "0xFA02610065351801FA02610065351801";

    /**
     * 加密方法
     *
     * @param src 源数据的字节数组
     * @return
     */
    public static byte[] encryptMode(byte[] src) {
        try {
            DESedeKeySpec deskey = new DESedeKeySpec(
                    build3DesKey(PASSWORD_CRYPT_KEY));
            SecretKey key = SecretKeyFactory.getInstance("DESede").generateSecret(deskey);
            Cipher c1 = Cipher.getInstance("DESede/ECB/NoPadding");
            c1.init(Cipher.ENCRYPT_MODE, key);
            return c1.doFinal(src);

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /**
     * 加密方法
     *
     * @param src 源数据的字节数组
     * @return
     */
    public static byte[] encryptMode(byte[] keyBytes,byte[] src) {
        try {
            DESedeKeySpec deskey = new DESedeKeySpec(
                    keyBytes);
            SecretKey key = SecretKeyFactory.getInstance("DESede").generateSecret(deskey);
            Cipher c1 = Cipher.getInstance("DESede/ECB/NoPadding");
            c1.init(Cipher.ENCRYPT_MODE, key);
            return c1.doFinal(src);
        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /**
     * 解密函数
     *
     * @param src 密文的字节数组
     * @return
     */
    public static byte[] decryptMode(byte[] src) {
        try {

//            DESedeKeySpec deskey = new DESedeKeySpec(
//                    build3DesKey(PASSWORD_CRYPT_KEY));
//            SecretKey key = SecretKeyFactory.getInstance(Algorithm).generateSecret(deskey);
//            Cipher c1 = Cipher.getInstance("DESede/CBC/Nopadding");
//            c1.init(Cipher.DECRYPT_MODE, key);
//            return c1.doFinal(src);
//
//            DESedeKeySpec spec = new DESedeKeySpec(build3DesKey(PASSWORD_CRYPT_KEY));
//            SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("desede");
//            Key deskey = keyfactory.generateSecret(spec);
//            //ecb
//            Cipher cipher = Cipher.getInstance("desede" + "/ECB/NoPadding");
//            cipher.init(Cipher.DECRYPT_MODE, deskey);

            //CBC
//            byte[] keyiv = new byte[8];
//            Cipher cipher = Cipher.getInstance("desede" + "/CBC/NoPadding");
//            IvParameterSpec ips = new IvParameterSpec(keyiv);
//            cipher.init(Cipher.ENCRYPT_MODE, deskey, ips);

//            return cipher.doFinal(src);

            DESedeKeySpec deskey = new DESedeKeySpec(
                    build3DesKey(PASSWORD_CRYPT_KEY));
            SecretKey key = SecretKeyFactory.getInstance("DESede").generateSecret(deskey);
            Cipher c1 = Cipher.getInstance("DESede/ECB/NoPadding");
            c1.init(Cipher.DECRYPT_MODE, key);
            return c1.doFinal(src);

        } catch (NoSuchAlgorithmException e1) {
            e1.printStackTrace();
        } catch (javax.crypto.NoSuchPaddingException e2) {
            e2.printStackTrace();
        } catch (Exception e3) {
            e3.printStackTrace();
        }
        return null;
    }

    /**
     * 生成秘钥
     *
     * @return
     */
    public static byte[] generateKey() {

        KeyGenerator keyGen = null;
        try {
            keyGen = KeyGenerator.getInstance(Algorithm); // 秘钥生成器
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        keyGen.init(16); // 初始秘钥生成器
        SecretKey secretKey = keyGen.generateKey(); // 生成秘钥
        return secretKey.getEncoded(); // 获取秘钥字节数组
    }

    /**
     * 根据字符串生成密钥24位的字节数组
     * 修正KEY長度，3DES的密鑰長度需為24Byte，不夠補0
     * java.security.InvalidKeyException ：通常是你的key長度不對
     *
     * @param keyStr
     * @return
     * @throws UnsupportedEncodingException
     */
    public static byte[] build3DesKey(String keyStr)
            throws UnsupportedEncodingException {
        byte[] key = new byte[24];
        byte[] temp = keyStr.getBytes("US-ASCII");

        if (key.length > temp.length) {
            System.arraycopy(temp, 0, key, 0, temp.length);
        } else {
            System.arraycopy(temp, 0, key, 0, key.length);
        }
//        byte[] result = new byte[]{0x77, 0x77, 0x77, 0x2e, 0x6c
//                , 0x65, 0x65, 0x6c, 0x65, 0x6e, 0x2e, 0x63, 0x6f, 0x6d, 0x31, 0x31, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

        byte[] result = new byte[]{0x77, 0x77, 0x77, 0x2e, 0x6c
                , 0x65, 0x65, 0x6c, 0x65, 0x6e, 0x2e, 0x63, 0x6f, 0x6d, 0x31, 0x31, 0x77, 0x77, 0x77, 0x2e, 0x6c
                , 0x65, 0x65, 0x6c};

        return result;
    }


    /**
     *
     */
}