package cn.com.heaton.bleLib.logic.bean;

import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.logic.HexUtil;

public class WhiteBean {
    public int cardType;//卡类型

    public int cardLength;//卡号长度
    public byte[] cardNo;//卡号，长度为卡号的长度


    public int year;//年
    public int month;
    public int day;
    public String formateTime;


    /**
     * 数据解析
     *
     * @param data
     * @param offset
     * @return
     */
    public static WhiteBean of(byte[] data, int offset) {

        WhiteBean whiteBean = new WhiteBean();
        L.i("WhiteBean", ">>>:" + HexUtil.encodeHexStr(data));
        //卡类型
        byte cardTypeB = data[offset];
        whiteBean.cardType = Integer.parseInt(HexUtil.Byte2Hex(cardTypeB), 16);
        offset++;

        //卡号长度
        byte length = data[offset];
        whiteBean.cardLength = Integer.parseInt(HexUtil.Byte2Hex(length), 16);
        offset++;

        //卡号
        byte[] cardNo = new byte[length];
        System.arraycopy(data, offset, cardNo, 0, length);
        whiteBean.cardNo = cardNo;
        offset = offset + length;


        //year
        byte yearB = data[offset];
        whiteBean.year = Integer.parseInt(HexUtil.Byte2Hex(yearB), 16) + 2000;
        offset++;

        //month
        byte month = data[offset];
        whiteBean.month = Integer.parseInt(HexUtil.Byte2Hex(month), 16);
        offset++;

        //day

        byte day = data[offset];
        whiteBean.day = Integer.parseInt(HexUtil.Byte2Hex(day), 16);
        offset++;

        whiteBean.formateTime();
        return whiteBean;
    }

    public void formateTime() {
        StringBuilder sbTime = new StringBuilder();
        sbTime.append(year)
                .append("年")
                .append(month)
                .append("月")
                .append(day)
                .append("日");
        formateTime = sbTime.toString();
    }

}
