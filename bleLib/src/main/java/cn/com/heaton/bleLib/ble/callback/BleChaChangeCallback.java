package cn.com.heaton.bleLib.ble.callback;

/**
 * 蓝牙特征值变化
 * Created by LiuLei on 2017/10/23.
 */
public abstract class BleChaChangeCallback<T> {
    /**
     * MCU data sent to the app when the data callback call is setNotify
     *
     * @param device         ble device object
     */
    public abstract void onChanged(T device, byte[] data);

}
