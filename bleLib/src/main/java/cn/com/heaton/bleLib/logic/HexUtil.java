package cn.com.heaton.bleLib.logic;

import java.io.UnsupportedEncodingException;

import static java.lang.Integer.parseInt;

/**
 * reference apache commons <a
 * href="http://commons.apache.org/codec/">http://commons.apache.org/codec/</a>
 *
 * @author Aub
 */
public class HexUtil {

    /**
     * 用于建立十六进制字符的输出的小写字符数组
     */
    private static final char[] DIGITS_LOWER = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};

    /**
     * 用于建立十六进制字符的输出的大写字符数组
     */
    private static final char[] DIGITS_UPPER = {'0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data byte[]
     * @return 十六进制char[]
     */
    public static char[] encodeHex(byte[] data) {
        return encodeHex(data, true);
    }

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data        byte[]
     * @param toLowerCase <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
     * @return 十六进制char[]
     */
    public static char[] encodeHex(byte[] data, boolean toLowerCase) {
        return encodeHex(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
    }

    /**
     * 将字节数组转换为十六进制字符数组
     *
     * @param data     byte[]
     * @param toDigits 用于控制输出的char[]
     * @return 十六进制char[]
     */
    protected static char[] encodeHex(byte[] data, char[] toDigits) {
        int l = data.length;
        char[] out = new char[l << 1];
        // two characters form the hex value.
        for (int i = 0, j = 0; i < l; i++) {
            out[j++] = toDigits[(0xF0 & data[i]) >>> 4];
            out[j++] = toDigits[0x0F & data[i]];
        }
        return out;
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data byte[]
     * @return 十六进制String
     */
    public static String encodeHexStr(byte[] data) {
        return encodeHexStr(data, true);
    }

    static public String Byte2Hex(Byte inByte)//1字节转2个Hex字符
    {
        return String.format("%02x", inByte).toUpperCase();
    }


    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data        byte[]
     * @param toLowerCase <code>true</code> 传换成小写格式 ， <code>false</code> 传换成大写格式
     * @return 十六进制String
     */
    public static String encodeHexStr(byte[] data, boolean toLowerCase) {
        return encodeHexStr(data, toLowerCase ? DIGITS_LOWER : DIGITS_UPPER);
    }

    /**
     * 将字节数组转换为十六进制字符串
     *
     * @param data     byte[]
     * @param toDigits 用于控制输出的char[]
     * @return 十六进制String
     */
    protected static String encodeHexStr(byte[] data, char[] toDigits) {
        return new String(encodeHex(data, toDigits));
    }

    /**
     * 将十六进制字符数组转换为字节数组
     *
     * @param data 十六进制char[]
     * @return byte[]
     * @throws RuntimeException 如果源十六进制字符数组是一个奇怪的长度，将抛出运行时异常
     */
    public static byte[] decodeHex(char[] data) {

        int len = data.length;

        if ((len & 0x01) != 0) {
            throw new RuntimeException("Odd number of characters.");
        }

        byte[] out = new byte[len >> 1];

        // two characters form the hex value.
        for (int i = 0, j = 0; j < len; i++) {
            int f = toDigit(data[j], j) << 4;
            j++;
            f = f | toDigit(data[j], j);
            j++;
            out[i] = (byte) (f & 0xFF);
        }

        return out;
    }

    /**
     * 将十六进制字符转换成一个整数
     *
     * @param ch    十六进制char
     * @param index 十六进制字符在字符数组中的位置
     * @return 一个整数
     * @throws RuntimeException 当ch不是一个合法的十六进制字符时，抛出运行时异常
     */
    protected static int toDigit(char ch, int index) {
        int digit = Character.digit(ch, 16);
        if (digit == -1) {
            throw new RuntimeException("Illegal hexadecimal character " + ch
                    + " at index " + index);
        }
        return digit;
    }

    static public byte HexToByte(String inHex) throws NumberFormatException//Hex字符串转byte
    {
        return (byte) parseInt(inHex, 16);
    }

    //转hex字符串转字节数组
    static public byte[] HexToByteArr(String inHex)//hex字符串转字节数组
    {
        int hexlen = inHex.length();
        byte[] result;
        if (isOdd(hexlen) == 1) {//奇数
            hexlen++;
            result = new byte[(hexlen / 2)];
            inHex = "0" + inHex;
        } else {//偶数
            result = new byte[(hexlen / 2)];
        }
        int j = 0;
        for (int i = 0; i < hexlen; i += 2) {
            try {
                result[j] = HexToByte(inHex.substring(i, i + 2));
            } catch (Exception e) {
                result[j] = (byte) parseInt("00", 16);
            }
            j++;
        }
        return result;
    }

    // 判断奇数或偶数，位运算，最后一位是1则为奇数，为0是偶数
    public static int isOdd(int num) {
        return num & 0x1;
    }

    /**
     * 数组倒序
     *
     * @param myByte
     * @return
     */
    public static byte[] reverse(byte[] myByte) {
        byte[] newByte = new byte[myByte.length];

        for (int i = 0; i < myByte.length; i++) {
            newByte[i] = myByte[myByte.length - 1 - i];
        }
        return newByte;
    }

    public static void main(String[] args) {
//        String data = "dc3ebb147db1a9224edd6dad72c6c37b";
//        byte[] src = HexUtil.HexToByteArr(data);
//        byte[] result = HttpURLConnHelper.loadByteFromLocal(src);
//        System.out.print(HexUtil.encodeHexStr(result));
//        System.out.print("\neb73b9de853ae454\n");
//        String time = "201801061004";
//        String device = "35448106";
//        String communityId = "1214235636757";
//        String result = HttpURLConnHelper.generateQrCodeString(time, communityId, device);
//        System.out.print("\nresult:---->" + result);


        byte[] buffer = new byte[11];

        //fa05 4c a1 52 5a 83  6c cc bb d5 4e e4

        //bbd54ee4

//        fa05 0b a1 00 07 e3 07 0c 0c 10 23 18


        buffer[0] = 0x0b;
        buffer[1] = (byte) 0xa1;
        buffer[2] = (byte) 0x00;
        buffer[3] = 0x07;
        buffer[4] = (byte) 0xe3;
        buffer[5] = (byte) 0x07;
        buffer[6] = (byte) 0x0c;
        buffer[7] = (byte) 0x0c;
        buffer[8] = (byte) 0x10;
        buffer[9] = (byte) 0x23;
        buffer[10] = (byte) 0x18;




        byte check = 0;
        for (int i = 0; i < buffer.length; i++) {

            check += buffer[i];

        }


//        byte[] data = Rc4.RC4Base(buffer, Rc4.RC4Key);
        System.out.println("揭秘后的数据：" + Byte2Hex(check));


//        byte[] time = new byte[2];
//        time[0] = 0x07;
//        time[1] = (byte) 0xe3;

//        time[2] = 0x0c;
//        time[3] = 0x0b;
//        time[4] = 0x0a;
//        time[5] = 0x1e;
//        time[6] = 0x0a;

//        int timeStr = 0xe3;
//        int value = Integer.parseInt("07e3", 16);
//        System.out.println("揭秘后的时间：" + value);



        //定义一个十进制值
        int valueTen = 1;
        //将其转换为十六进制并输出
        String strHex = Integer.toHexString(valueTen);
        System.out.println(valueTen + " [十进制]---->[十六进制] " + strHex);
        //将十六进制格式化输出
        String strHex2 = String.format("%08x",valueTen);
        System.out.println(valueTen + " [十进制]---->[十六进制] " + strHex2);

    }

    public static byte[] getPrivateKey() {
        String privateKey = "www.leelen.com11";
        byte[] result = new byte[16];
        try {
            result = privateKey.getBytes("US-ASCII");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return result;

    }

    //byte 数组与 int 的相互转换
    public static int byteArrayToInt(byte[] b) {
        return b[3] & 0xFF |
                (b[2] & 0xFF) << 8 |
                (b[1] & 0xFF) << 16 |
                (b[0] & 0xFF) << 24;
    }


    public static int byteToInt(byte b) {
        //Java 总是把 byte 当做有符处理；我们可以通过将其和 0xFF 进行二进制与得到它的无符值
        return b & 0xFF;
    }
}
