package cn.com.heaton.bleLib.logic.notify;

/**
 * Created by bryan on 2019/5/21.
 */
public abstract class OpenDoorCallBack {

    public abstract void openSuccess();

    //失败的具体编码 失败：0x01 容量已满：0x02 无效卡号：0x03，指令超时：0x04
    public abstract void openFail(byte code);


}
