package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.BleHandler;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.logic.command.Command;

import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.notify.OpenDoorCallBack;

/**
 * 施工开门命令
 * Created by bryan on 2018/1/3 0003.
 */
public class CmdRequestOpenDoor extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private OpenDoorCallBack openDoorCallBack;
    private byte[] mSendBuffer;

    public CmdRequestOpenDoor(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback, OpenDoorCallBack openCallBack) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        this.openDoorCallBack = openCallBack;
        initData();
    }

    private void initData() {
        byte[] cmd = new byte[3];
        cmd[0] = 0x04;
        cmd[1] = 0x08;
        cmd[2] = 0x08;
        mSendBuffer = combineData(cmd, cmd.length);
    }

    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条施工开门命令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
