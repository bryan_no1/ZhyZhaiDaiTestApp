package cn.com.heaton.bleLib.logic.notify;

/**
 * Created by bryan on 2019/5/21.
 */
public abstract class AuthCallBack {

    public abstract void authSuccess();

    public abstract void authFail();
}
