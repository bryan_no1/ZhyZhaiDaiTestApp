package cn.com.heaton.bleLib.logic.command.impl;


import cn.com.heaton.bleLib.ble.Ble;
import cn.com.heaton.bleLib.ble.L;
import cn.com.heaton.bleLib.ble.callback.BleWriteCallback;
import cn.com.heaton.bleLib.ble.model.BleDevice;
import cn.com.heaton.bleLib.logic.command.Command;

/**
 * 请求进行设备授权，设备连接的时候需要完成此次认证
 */
public class AuthKeyRequest extends Command {

    private Ble<BleDevice> mBle;
    private BleDevice mDevice;
    private BleWriteCallback mCallBack;
    private byte[] mSendBuffer;

    public AuthKeyRequest(Ble<BleDevice> ble, BleDevice device, BleWriteCallback callback, byte[] buffer) {
        mBle = ble;
        mDevice = device;
        mCallBack = callback;
        mSendBuffer = buffer;
    }


    @Override
    protected boolean execute() {
        L.i(TAG, "发送了一条完成授权认证命令");
        return mSendBuffer != null && mBle.write(mDevice, mSendBuffer, mCallBack);
    }
}
