package cn.com.heaton.bleLib.logic.notify;

import cn.com.heaton.bleLib.logic.bean.RecordBean;

/**
 * 开门记录回调
 * Created by bryan on 2019/5/21.
 */
public abstract class OnRecordCallBack {

    public abstract void onSuccess(RecordBean recordBean);

    public abstract void onFail(String failReason);
}
